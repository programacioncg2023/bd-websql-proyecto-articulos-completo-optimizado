let salir_busq = document.querySelector("#salir_busqueda");
salir_busq.disabled = true;
// salir_busq.style.display = 'none';
//ASIGNACIÓN DE VARIABLES A ELEMENTOS DEL DOM
let titulo = document.querySelector("#titulo");
let seccion_producto = document.querySelector("#producto");
let descripcion = document.querySelector("#desc");
let url_imagen = document.querySelector("#dirImg");
let boton_uno = document.querySelector("#btn_uno");
let boton_dos = document.querySelector("#btn_dos");

(function () {
    if (!window.openDatabase) {
        alert("Este navegador no soporta el API WebSql");
    }
})();

let dbComercio, query;

function openDB() {
    console.log("Abriendo base de datos");
    let db_name = "dbComercio";
    let db_version = "1.0";
    let db_description = "Base de datos del comercio";
    let db_size = 2048;
    dbComercio = openDatabase(db_name, db_version, db_description, db_size);
}

function creaTabla() {
    console.log("Creando tabla");
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "CREATE TABLE IF NOT EXISTS Articulos (id integer primary key autoincrement, descripcion, imagen)"
        )
    });
}

let producto = {
    codigo: 0,
    descripcion: "",
    imagen: "",
};

let des, img;   //PARA LA VALIDACIÓN DE LOS INPUTS
let btns;       //VARIABLE QUE RECIBE SI SE MUESTRAN LOS ARTICULOS CON BOTONES O NO

function agregar() {
    console.log("Agregando registro");
    creaTabla(); //POR SI HACEMOS UN DROP DE LA TABLA
    dbComercio.transaction(function (tx) {
        // ID NO SE CARGA PORQUE ES AUTOINCREMENT
        des = descripcion.value.trim();
        img = url_imagen.value.trim();
        if (des.length === 0 || img.length === 0) return;

        producto.descripcion = des;
        producto.imagen = img;

        tx.executeSql(
            "INSERT INTO Articulos (descripcion, imagen) VALUES (?,?)",
            [producto.descripcion, producto.imagen],
            //itemInserted
            function (tx, result) {
                console.info(
                    "Registro ingresado satisfactoriamente, Id: ",
                    result.insertID
                );
            },
            function (tx, error) {
                console.log(error);
            }
        );
    });

    muestraTabla("con botones");
}
// function itemInserted(tx, results) {
//     console.log("id:", results.insertId);
// }

let cantidad;
//AHORA MUESTRATABLA() LEE TODOS LOS ARTICULOS DE LA TABLA Y LLAMA A ARMATEMPLATE
function muestraTabla(btns) {
    console.log("Entró en la select de muestraTabla");
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "SELECT * FROM Articulos",
            [],
            function (tx, results) {
                cantidad = results.rows.length;
                armaTemplate(results, btns);
            },
            function (tx, error) {
                console.log(error);
            }
        );
    });
}

//ARMA EL TEMPLATE CON LOS BOTONES
function armaTemplate(results, btns) {
    let template = "",
        row;

    for (let i = 0; i < cantidad; i++) {
        row = results.rows.item(i);
        producto.id = row.id;
        producto.descripcion = row.descripcion;
        producto.imagen = row.imagen;
        if (btns === "con botones") {
            template += `<article>
                            <div class="trash" onclick="eliminarItem(${producto.id})"><img src="eliminar.png"></div>
                            <div class="edit" onclick="editarItem(${producto.id})"><img src="edit1.png"></div>
                            <h3 class="descripcion">${producto.descripcion}</h3>
                            <img src="${producto.imagen}" class="imagen">
                        </article>`;
        } else {
            template += `<article>
                            <h3 class="descripcion">${producto.descripcion}</h3>
                            <img src="${producto.imagen}" class="imagen">
                        </article>`;
        }
    }
    document.querySelector("#producto").innerHTML = template;
}

//LISTADO LEE LA TABLA, PASA LOS ARTICULOS A LOCALSTORAGE, Y LLAMA A RESULTADOS.HTML
function listado() {
    let arrayProductos = [],
        row;
    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * FROM Articulos",
            [],
            function (tx, results) {
                cantidad = results.rows.length;
                for (let i = 0; i < cantidad; i++) {
                    row = results.rows.item(i);
                    console.log(row);
                    arrayProductos[i] = row;
                }
                if (cantidad > 0) {
                    localStorage.setItem(
                        "articulos",
                        JSON.stringify(arrayProductos)
                    );
                    location.href = "resultados.html";
                }

            },
            function (tx, error) {
                console.log(error);
            }
        );
    });
}

//ELIMINARITEM() BORRA EL ARTICULO INDICADO, MEDIANTE SU ID, Y LLAMA A MUESTRATABLA()
function eliminarItem(nroProd) {
    console.log("Borrando registro: ", nroProd);

    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "DELETE FROM Articulos WHERE id=?",
            [nroProd],
            function (tx, result) {
                console.log(result);
                console.info("Registro borrado satisfactoriamente!");
            },
            function (tx, error) {
                console.log(error);
            }
        );
    }, null);

    muestraTabla("con botones");
}

//EDITARITEM() RECIBE EL ID, BUSCA EL ARTICULO ESPECIFICO EN LA TABLA
//Y MUESTRA SU DESCRIPCIÓN E IMAGEN EN LOS INPUTS
//MUESTRA LA TABLA SIN BOTONES EN LA INTERFAZ PARA QUE NO EDITEN
//REEMPLAZA LOS BOTONES POR MODIFICAR Y CANCELAR
function editarItem(nroProd) {
    console.log("Actualizando registro: ", nroProd);
    muestraTabla("sin botones");
    document.querySelector("#titulo").innerHTML = "Edición de Producto";
    boton_uno.value = "Modificar";
    boton_uno.classList.add("color_green");
    boton_dos.value = "Cancelar";
    boton_dos.classList.add("color_red");
    boton_uno.setAttribute("onclick", `modificar(${nroProd})`);
    boton_dos.setAttribute("onclick", "limpiar_cancelar()");

    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "SELECT * FROM Articulos WHERE id=?",
            [nroProd],
            function (tx, results) {
                cantidad = results.rows.length;
                if (cantidad !== 1)
                    console.log(
                        "Error, se encontraron más de un registro iguales en la tabla"
                    );

                producto = results.rows.item(0);

                descripcion.value = producto.descripcion;
                url_imagen.value = producto.imagen;
            },
            function (tx, error) {
                console.log(error);
            }
        );
    });
}


//MUESTRA TABLA SIN BOTONES
// function muestraTablaSinBtns() {
//     console.log("Mostrando tabla sin botones");
//     dbComercio.transaction(function (tx) {
//         tx.executeSql("SELECT * FROM Articulos", [], function (tx, results) {
//             cantidad = results.rows.length;
//             armaTemplateSinBtns(results);
//         });
//     });
// }

//ARMA EL TEMPLATE SIN BOTONES
// function armaTemplateSinBtns(results) {
//     let template = '', row;

//     for (let i = 0; i < cantidad; i++) {
//         row = results.rows.item(i);

//         producto.id = row.id;
//         producto.descripcion = row.descripcion;
//         producto.imagen = row.imagen;

//         template += `<article>
//                         <h3 class="descripcion">${producto.descripcion}</h3>
//                         <img src="${producto.imagen}" class="imagen">
//                     </article>`
//     }
//     document.querySelector("#producto").innerHTML = template;
// }

function limpiar_cancelar() {
    descripcion.value = "";
    url_imagen.value = "";
    titulo.innerHTML = "Nuevo Producto";
    boton_uno.value = "Agregar";
    boton_uno.classList.remove("color_green");
    boton_dos.value = "Listado";
    boton_dos.classList.remove("color_red");
    boton_uno.setAttribute("onclick", "agregar()");
    boton_dos.setAttribute("onclick", "listado()");
    muestraTabla("con botones");
}

function modificar(nroProd) {
    console.log("Modificando registro: ", nroProd);
    des = descripcion.value.trim();
    img = url_imagen.value.trim();
    if (des.length === 0 || img.length === 0) return;

    producto = {
        id: nroProd,
        descripcion: des,
        imagen: img,
    };

    dbComercio.transaction(function (tx) {
        console.log(producto.descripcion, " - ", producto.imagen);
        tx.executeSql(
            "UPDATE Articulos SET descripcion=?, imagen=? WHERE id=?",
            [producto.descripcion, producto.imagen, producto.id],
            function (tx, result) {
                console.log(result);
                console.info("Registro actualizado satisfactoriamente!");
            },
            function (tx, error) {
                console.log(error);
            }
        );
    }, null);
    limpiar_cancelar();
}

function busqueda() {
    document.querySelector("#agrego").classList.add("disable");
    salir_busq.disabled = false;
    let aBuscar = document.querySelector("#aBuscar").value;
    if (aBuscar.trim().length === 0) {
        muestraTabla("sin botones");
    } else {
        console.log("Leyendo con select lo pedido");
        dbComercio.transaction(function (tx) {
            let q = "SELECT * FROM Articulos WHERE descripcion LIKE ?";
            console.log(q);
            tx.executeSql(q, [aBuscar.trim() + "%"],
                function (tx, results) {
                    cantidad = results.rows.length;
                    console.log(cantidad);
                    armaTemplate(results, "sin botones");
                },
                function (tx, error) {
                    console.log(error);
                }
            );
        });
    }
}

function salir_busqueda() {
    aBuscar.value = "";
    muestraTabla("con botones");
    document.querySelector("#agrego").classList.remove("disable");
    salir_busq.disabled = true;
}

function dropTable() {
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "DROP TABLE Articulos",
            [],
            function (tx, results) {
                console.log("Table Dropped");
            },
            function (tx, error) {
                console.error("Error capturado: " + error.message);
            }
        );
    });
    document.querySelector("#producto").innerHTML = "";
}

function main() {
    openDB();
    creaTabla();
    muestraTabla("con botones");
}

main();
